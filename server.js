const express = require('express');
const app = express();
const router = express.Router();

const request = require('superagent');

const api = 'https://api.behance.net/v2';
const apiKey = 'eTRJzU94JmMDxoy5ilNLt5PdYFhJeuh1';

const port = process.env.PORT || 5000;

//TODO: make a middleware for the try/catch
app.get('/api/search', async (req, res) => {
    const { term } = req.query;

    try {
        const response = await request(`${api}/users?q=${term}&client_id=${apiKey}`);
        const data = JSON.parse(response.text);

        res.send({
            users: data.users
        });
    } catch (err) {
        res.send({ hasError: true, errorCode: err.status });
    }
});

app.get('/api/user/:username', async (req, res) => {
    const { username } = req.params;
    try {
        const response = await request(`${api}/users/${username}/?client_id=${apiKey}`);
        const data = JSON.parse(response.text);
        res.send({
            ...data.user
        });
    } catch (err) {
        res.send({ hasError: true, errorCode: err.status });
    }
});

app.get('/api/user/:username/work_experience', async (req, res) => {
    const { username } = req.params;

    try {
        const response = await request(`${api}/users/${username}/work_experience?client_id=${apiKey}`);
        const data = JSON.parse(response.text);

        res.send({
            data: data.work_experience
        });
    } catch (err) {
        res.send({ hasError: true, errorCode: err.status });
    }
});

app.get('/api/user/:username/projects', async (req, res) => {
    const { username } = req.params;

    try {
        const response = await request(`${api}/users/${username}/projects?client_id=${apiKey}`);
        const data = JSON.parse(response.text);
        console.log(data);
        res.send({
            data: data.projects.splice(0, 5)
        });
    } catch (err) {
        res.send({ hasError: true, errorCode: err.status });
    }
});

app.get('/api/user/:username/followers', async (req, res) => {
    const { username } = req.params;

    try {
        const response = await request(`${api}/users/${username}/followers?client_id=${apiKey}`);
        const data = JSON.parse(response.text);
        console.log(data);
        res.send({
            data: data.followers.splice(0, 10)
        });
    } catch (err) {
        res.send({ hasError: true, errorCode: err.status });
    }
});

app.get('/api/user/:username/following', async (req, res) => {
    const { username } = req.params;

    try {
        const response = await request(`${api}/users/${username}/following?client_id=${apiKey}`);
        const data = JSON.parse(response.text);

        res.send({
            data: data.following.splice(0, 10)
        });
    } catch (err) {
        res.send({ hasError: true, errorCode: err.status });
    }
});

app.listen(port, () => {
    console.log('Behance Api runs at', port);
});
