import Immutable from 'immutable';

const initialState = {
    username: null,
    workexp: null,
    projects: null,
    followers: null,
    following: null,
    data: null
};

export default function(state = Immutable.fromJS(initialState), action = {}) {
    switch (action.type) {
        case 'REQUEST_USER':
            return state.set('username', Immutable.fromJS(action.data));
        case 'RECEIVE_USER':
            return state.set('data', Immutable.fromJS(action.data));
        case 'RECEIVE_WORK_EXP':
            return state.set('workexp', Immutable.fromJS(action.data.data));
        case 'RECEIVE_PROJECTS':
            return state.set('projects', Immutable.fromJS(action.data.data));
        case 'RECEIVE_FOLLOWERS':
            return state.set('followers', Immutable.fromJS(action.data.data));
        case 'RECEIVE_FOLLOWING':
            return state.set('following', Immutable.fromJS(action.data.data));

        default:
            return state;
    }
}
