import Immutable from 'immutable';

const initialState = {
    isInFlight: false,
    data: null,
    term: null
};

export default function(state = Immutable.fromJS(initialState), action = {}) {
    switch (action.type) {
        case 'REQUEST_USER_SEARCH':
            return state
                .set('isInFlight', true)
                .set('term', Immutable.fromJS(action.data.term))
                .set('data', Immutable.List());

        case 'RECEIVE_USER_SEARCH':
            return state.set('data', Immutable.fromJS(action.data)).set('isInFlight', false);

        default:
            return state;
    }
}
