import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { enzyme } from './testSetup';
import store from './store';

it('renders without crashing', () => {
    const wrapper = enzyme.shallow(<App store={store} />);
    expect(wrapper.length).toEqual(1);
});
