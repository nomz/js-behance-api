import store from '../store';
import request from 'superagent';

export async function search(term) {
    store.dispatch({
        type: 'REQUEST_USER_SEARCH',
        data: { term }
    });

    const response = await request.get(`/api/search?term=${term}`);
    return response.body.users;
}

export async function getUser(username) {
    const response = await request.get(`/api/user/${username}`);
    store.dispatch({ type: 'RECEIVE_USER', data: response.body });
    return response.body;
}

export async function getProjects(username) {
    const response = await request.get(`/api/user/${username}/projects`);
    store.dispatch({ type: 'RECEIVE_PROJECTS', data: response.body });
    return response.body;
}

export async function getWorkExp(username) {
    const response = await request.get(`/api/user/${username}/work_experience`);
    store.dispatch({ type: 'RECEIVE_WORK_EXP', data: response.body });
    return response.body;
}

export async function getFollowers(username) {
    const response = await request.get(`/api/user/${username}/followers`);
    store.dispatch({ type: 'RECEIVE_FOLLOWERS', data: response.body });
    return response.body;
}

export async function getFollowing(username) {
    const response = await request.get(`/api/user/${username}/following`);
    store.dispatch({ type: 'RECEIVE_FOLLOWING', data: response.body });
    return response.body;
}
