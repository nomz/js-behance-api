import React, { Component } from 'react';
import { Switch } from 'react-router';
import Search from './components/Search/Search';
import UserCard from './components/UserCard/UserCard';
import './App.scss';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Search />
                <UserCard />
            </div>
        );
    }
}

export default App;
