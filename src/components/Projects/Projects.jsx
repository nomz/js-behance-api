import { getProjects } from '../../module/load';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import './Projects.scss';

class Projects extends Component {
    componentWillReceiveProps(nextProp) {
        if (this.props.username !== nextProp.username) {
            getProjects(nextProp.username);
        }
    }

    render() {
        if (!this.props.projects) return null;
        const selectedProjects = this.props.projects.splice(0, 3);
        return (
            <div className="project-container">
                <h3>Projects:</h3>
                {selectedProjects.map((project, i) => (
                    <div className="project" key={`${project.get('name')}-${i}`}>
                        <img className="project-img" src={project.get('covers').get('115')} alt="" />
                        <a href={project.get('url')}>{project.get('name')}</a>
                    </div>
                ))}
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {
        projects: store.user.get('projects')
    };
};

export default connect(mapStoreToProps)(Projects);
