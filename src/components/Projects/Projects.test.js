import React from 'react';
import ReactDOM from 'react-dom';
import { enzyme } from '../../testSetup';
import store from '../../store';
import Projects from './Projects';
import Immutable from 'immutable';

const projects = [
    {
        name: 'SUPERGIRL',
        url: 'https://www.behance.net/gallery/57597017/SUPERGIRL',
        covers: {
            '115': 'https://mir-s3-cdn-cf.behance.net/projects/115/6adc1b57597017.Y3JvcCwyMzI3LDE4MjEsNjAzLDA.png'
        }
    }
];

it('renders without crashing when there is data', () => {
    const wrapper = enzyme.shallow(<Projects store={store} projects={Immutable.fromJS(projects)} />);
    expect(wrapper.length).toEqual(1);
});

it('renders without crashing when there is no data', () => {
    const wrapper = enzyme.shallow(<Projects store={store} />);
    expect(wrapper.length).toEqual(1);
});
