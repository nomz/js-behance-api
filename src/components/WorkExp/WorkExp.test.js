import React from 'react';
import ReactDOM from 'react-dom';
import { enzyme } from '../../testSetup';
import WorkExp from './WorkExp';
import store from '../../store';
import Immutable from 'immutable';

const workexp = [
    {
        position: 'Senior Visual Designer ',
        organization: 'Vagado',
        location: 'United Kingdom'
    }
];

it('renders without crashing with data', () => {
    const wrapper = enzyme.shallow(
        <WorkExp store={store} username="cecilia_marzocchi" workexp={Immutable.fromJS(workexp)} />
    );
    expect(wrapper.length).toEqual(1);
});

it('renders without crashing when there is no data', () => {
    const wrapper = enzyme.shallow(<WorkExp store={store} username="cecilia_marzocchi" />);
    expect(wrapper.length).toEqual(1);
});
