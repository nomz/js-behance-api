import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getWorkExp } from '../../module/load';
import './WorkExp.scss';
class WorkExp extends Component {
    async componentWillReceiveProps(nextProp) {
        if (this.props.username !== nextProp.username) {
            await getWorkExp(nextProp.username);
        }
    }

    render() {
        if (!this.props.workexp) return null;
        return (
            <div className="work-exp">
                {this.props.workexp.size !== 0 ? <h3>WORK EXPERIENCE</h3> : null}
                {this.props.workexp.map((exp, i) => (
                    <div className="work-exp-item" key={`${exp.get('organization')}-${i}`}>
                        <h4>{exp.get('position')}</h4>
                        <div>Location: {exp.get('location')}</div>
                        <div>Organization: {exp.get('organization')}</div>
                    </div>
                ))}
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {
        username: store.user.get('username'),
        workexp: store.user.get('workexp')
    };
};

export default connect(mapStoreToProps)(WorkExp);
