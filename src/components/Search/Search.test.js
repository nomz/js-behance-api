import React from 'react';
import ReactDOM from 'react-dom';
import { enzyme } from '../../testSetup';
import store from '../../store';
import Search from './Search';

it('renders without crashing', () => {
    const wrapper = enzyme.shallow(<Search store={store} />);
    expect(wrapper.length).toEqual(1);
});
