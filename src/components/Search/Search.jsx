import React, { Component } from 'react';
import { connect } from 'react-redux';
import { search } from '../../module/load';
import store from '../../store';
import 'react-select/scss/default.scss';
import Select from 'react-select';
import './Search.scss';

class Search extends Component {
    state = {
        value: ''
    };

    async getUsers(input) {
        if (!input) {
            return Promise.resolve({ options: [] });
        }

        const response = await search(input);
        return { options: response };
    }

    onChange(value) {
        store.dispatch({ type: 'REQUEST_USER', data: value.username }, { type: 'RECEIVE_USER_SEARCH', data: value });
        this.setState({ value });
    }

    render() {
        const AsyncSelect = Select.Async;
        return (
            <div className="search">
                <AsyncSelect
                    multi={false}
                    loadOptions={e => this.getUsers(e)}
                    onChange={e => this.onChange(e)}
                    valueKey="username"
                    labelKey="display_name"
                    value={this.state.value}
                />
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {
        term: store.search.get('term'),
        searchData: store.search.get('data'),
        username: store.user.get('username')
    };
};

export default connect(mapStoreToProps)(Search);
