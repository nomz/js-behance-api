import { getFollowing } from '../../module/load';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import './Following.scss';

class Following extends Component {
    componentWillReceiveProps(nextProp) {
        if (this.props.username !== nextProp.username) {
            getFollowing(nextProp.username);
        }
    }

    render() {
        if (!this.props.following) return null;
        const followings = this.props.following;
        return (
            <div className="follower-container">
                <h3>Followings:</h3>
                {followings.map((following, i) => (
                    <div className="following" key={`${following.get('display_name')}-${i}`}>
                        <img className="following-img" src={following.get('images').get('50')} alt="following image" />
                        <h6>{following.get('display_name')}</h6>
                    </div>
                ))}
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {
        following: store.user.get('following')
    };
};

export default connect(mapStoreToProps)(Following);
