import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUser } from '../../module/load';
import Projects from '../Projects/Projects';
import WorKExp from '../WorkExp/WorkExp';
import Followers from '../Followers/Followers';
import Following from '../Following/Following';

import './UserCard.scss';

class UserCard extends Component {
    async componentWillReceiveProps(nextProp) {
        if (this.props.username !== nextProp.username) {
            await getUser(nextProp.username);
        }
    }

    render() {
        if (!this.props.data) return null;
        const data = this.props.data;
        return (
            <div className="user-card">
                <img src={`${data.get('images').get('50')}`} alt="user image" />
                <div className="name">{data.get('display_name')}</div>
                <div className="influence">
                    <div>Followers: {data.get('stats').get('followers')}</div>
                    <div>Following: {data.get('stats').get('following')}</div>
                </div>
                <Projects username={this.props.username} />
                <WorKExp username={this.props.username} />
                <Followers username={this.props.username} />
                <Following username={this.props.username} />
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {
        name: store.user.get('name'),
        id: store.user.get('id'),
        data: store.user.get('data'),
        searchData: store.search.get('data'),
        username: store.user.get('username')
    };
};

export default connect(mapStoreToProps)(UserCard);
