import React from 'react';
import ReactDOM from 'react-dom';
import { enzyme } from '../../testSetup';
import store from '../../store';
import UserCard from './UserCard';

it('renders without crashing', () => {
    const wrapper = enzyme.shallow(<UserCard store={store} />);
    expect(wrapper.length).toEqual(1);
});
