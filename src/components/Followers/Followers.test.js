import React from 'react';
import ReactDOM from 'react-dom';
import { enzyme } from '../../testSetup';
import store from '../../store';
import Followers from './Followers';
import Immutable from 'immutable';

const following = [
    {
        images: {
            '50': 'https://mir-s3-cdn-cf.behance.net/user/50/c526cf2071129.59d26b48d9805.jpg'
        },
        display_name: 'Carol Pagano'
    }
];

it('renders without crashing when there is data', () => {
    const wrapper = enzyme.shallow(<Followers store={store} following={Immutable.fromJS(following)} />);
    expect(wrapper.length).toEqual(1);
});

it('renders without crashing when there is no data', () => {
    const wrapper = enzyme.shallow(<Followers store={store} />);
    expect(wrapper.length).toEqual(1);
});
