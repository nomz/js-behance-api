import { getFollowers } from '../../module/load';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import './Followers.scss';

class Followers extends Component {
    componentWillReceiveProps(nextProp) {
        if (this.props.username !== nextProp.username) {
            getFollowers(nextProp.username);
        }
    }

    render() {
        if (!this.props.followers) return null;
        const followers = this.props.followers;
        return (
            <div className="follower-container">
                <h3>Followers:</h3>
                {followers.map((follower, i) => (
                    <div className="follower" key={`${follower.get('display_name')}-${i}`}>
                        <img className="follower-img" src={follower.get('images').get('50')} alt="follower image" />
                        <h6>{follower.get('display_name')}</h6>
                    </div>
                ))}
            </div>
        );
    }
}

const mapStoreToProps = store => {
    return {
        followers: store.user.get('followers')
    };
};

export default connect(mapStoreToProps)(Followers);
