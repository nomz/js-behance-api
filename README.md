## Behance Code Challenge

Available Scripts

### `yarn start`

Runs the app at http://localhost:3000 as well as the node server at http:localhost:5000

### `yarn test`

Runs the tests

## Run locally

```
git clone https://bitbucket.org/nomz/js-behance-api.git

cd js-behance-api

yarn

yarn start
```

## TODO

Better test coverage. Super basic testing is provided at the moment!
